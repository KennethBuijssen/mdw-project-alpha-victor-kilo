﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34209
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace mdw.client.GameService {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="GameRoom", Namespace="http://schemas.datacontract.org/2004/07/mdw.service.portal")]
    [System.SerializableAttribute()]
    public partial class GameRoom : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string AddresField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private mdw.client.GameService.GameRoomInfo GameRoomInfoField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Addres {
            get {
                return this.AddresField;
            }
            set {
                if ((object.ReferenceEquals(this.AddresField, value) != true)) {
                    this.AddresField = value;
                    this.RaisePropertyChanged("Addres");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public mdw.client.GameService.GameRoomInfo GameRoomInfo {
            get {
                return this.GameRoomInfoField;
            }
            set {
                if ((object.ReferenceEquals(this.GameRoomInfoField, value) != true)) {
                    this.GameRoomInfoField = value;
                    this.RaisePropertyChanged("GameRoomInfo");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="GameRoomInfo", Namespace="http://schemas.datacontract.org/2004/07/mdw.service.portal")]
    [System.SerializableAttribute()]
    public partial class GameRoomInfo : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int CurrentPlayersField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int MaxPlayersField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string NameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private bool ObservableField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int CurrentPlayers {
            get {
                return this.CurrentPlayersField;
            }
            set {
                if ((this.CurrentPlayersField.Equals(value) != true)) {
                    this.CurrentPlayersField = value;
                    this.RaisePropertyChanged("CurrentPlayers");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int MaxPlayers {
            get {
                return this.MaxPlayersField;
            }
            set {
                if ((this.MaxPlayersField.Equals(value) != true)) {
                    this.MaxPlayersField = value;
                    this.RaisePropertyChanged("MaxPlayers");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Name {
            get {
                return this.NameField;
            }
            set {
                if ((object.ReferenceEquals(this.NameField, value) != true)) {
                    this.NameField = value;
                    this.RaisePropertyChanged("Name");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool Observable {
            get {
                return this.ObservableField;
            }
            set {
                if ((this.ObservableField.Equals(value) != true)) {
                    this.ObservableField = value;
                    this.RaisePropertyChanged("Observable");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="SessionToken", Namespace="http://schemas.datacontract.org/2004/07/mdw.service.authentication")]
    [System.SerializableAttribute()]
    public partial class SessionToken : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string EmailField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string TokenField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Email {
            get {
                return this.EmailField;
            }
            set {
                if ((object.ReferenceEquals(this.EmailField, value) != true)) {
                    this.EmailField = value;
                    this.RaisePropertyChanged("Email");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Token {
            get {
                return this.TokenField;
            }
            set {
                if ((object.ReferenceEquals(this.TokenField, value) != true)) {
                    this.TokenField = value;
                    this.RaisePropertyChanged("Token");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Game", Namespace="http://schemas.datacontract.org/2004/07/mdw.service.game")]
    [System.SerializableAttribute()]
    public partial class Game : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int IDField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private mdw.client.GameService.GameRoom gameRoomField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int ID {
            get {
                return this.IDField;
            }
            set {
                if ((this.IDField.Equals(value) != true)) {
                    this.IDField = value;
                    this.RaisePropertyChanged("ID");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public mdw.client.GameService.GameRoom gameRoom {
            get {
                return this.gameRoomField;
            }
            set {
                if ((object.ReferenceEquals(this.gameRoomField, value) != true)) {
                    this.gameRoomField = value;
                    this.RaisePropertyChanged("gameRoom");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="GameService.IGameService")]
    public interface IGameService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IGameService/createGame", ReplyAction="http://tempuri.org/IGameService/createGameResponse")]
        mdw.client.GameService.Game createGame(mdw.client.GameService.GameRoom gameRoom, mdw.client.GameService.SessionToken sessionToken);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IGameService/createGame", ReplyAction="http://tempuri.org/IGameService/createGameResponse")]
        System.Threading.Tasks.Task<mdw.client.GameService.Game> createGameAsync(mdw.client.GameService.GameRoom gameRoom, mdw.client.GameService.SessionToken sessionToken);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IGameService/joinGame", ReplyAction="http://tempuri.org/IGameService/joinGameResponse")]
        mdw.client.GameService.Game joinGame(mdw.client.GameService.GameRoom gameRoom, mdw.client.GameService.SessionToken sessionToken);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IGameService/joinGame", ReplyAction="http://tempuri.org/IGameService/joinGameResponse")]
        System.Threading.Tasks.Task<mdw.client.GameService.Game> joinGameAsync(mdw.client.GameService.GameRoom gameRoom, mdw.client.GameService.SessionToken sessionToken);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IGameService/leaveGame", ReplyAction="http://tempuri.org/IGameService/leaveGameResponse")]
        void leaveGame(mdw.client.GameService.Game game, mdw.client.GameService.SessionToken sessionToken);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IGameService/leaveGame", ReplyAction="http://tempuri.org/IGameService/leaveGameResponse")]
        System.Threading.Tasks.Task leaveGameAsync(mdw.client.GameService.Game game, mdw.client.GameService.SessionToken sessionToken);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IGameService/playGame", ReplyAction="http://tempuri.org/IGameService/playGameResponse")]
        bool playGame(mdw.client.GameService.Game game, mdw.client.GameService.SessionToken sessionToken);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IGameService/playGame", ReplyAction="http://tempuri.org/IGameService/playGameResponse")]
        System.Threading.Tasks.Task<bool> playGameAsync(mdw.client.GameService.Game game, mdw.client.GameService.SessionToken sessionToken);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IGameServiceChannel : mdw.client.GameService.IGameService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class GameServiceClient : System.ServiceModel.ClientBase<mdw.client.GameService.IGameService>, mdw.client.GameService.IGameService {
        
        public GameServiceClient() {
        }
        
        public GameServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public GameServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public GameServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public GameServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public mdw.client.GameService.Game createGame(mdw.client.GameService.GameRoom gameRoom, mdw.client.GameService.SessionToken sessionToken) {
            return base.Channel.createGame(gameRoom, sessionToken);
        }
        
        public System.Threading.Tasks.Task<mdw.client.GameService.Game> createGameAsync(mdw.client.GameService.GameRoom gameRoom, mdw.client.GameService.SessionToken sessionToken) {
            return base.Channel.createGameAsync(gameRoom, sessionToken);
        }
        
        public mdw.client.GameService.Game joinGame(mdw.client.GameService.GameRoom gameRoom, mdw.client.GameService.SessionToken sessionToken) {
            return base.Channel.joinGame(gameRoom, sessionToken);
        }
        
        public System.Threading.Tasks.Task<mdw.client.GameService.Game> joinGameAsync(mdw.client.GameService.GameRoom gameRoom, mdw.client.GameService.SessionToken sessionToken) {
            return base.Channel.joinGameAsync(gameRoom, sessionToken);
        }
        
        public void leaveGame(mdw.client.GameService.Game game, mdw.client.GameService.SessionToken sessionToken) {
            base.Channel.leaveGame(game, sessionToken);
        }
        
        public System.Threading.Tasks.Task leaveGameAsync(mdw.client.GameService.Game game, mdw.client.GameService.SessionToken sessionToken) {
            return base.Channel.leaveGameAsync(game, sessionToken);
        }
        
        public bool playGame(mdw.client.GameService.Game game, mdw.client.GameService.SessionToken sessionToken) {
            return base.Channel.playGame(game, sessionToken);
        }
        
        public System.Threading.Tasks.Task<bool> playGameAsync(mdw.client.GameService.Game game, mdw.client.GameService.SessionToken sessionToken) {
            return base.Channel.playGameAsync(game, sessionToken);
        }
    }
}
