﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34209
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace mdw.client.ChatService {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ChatService.IChatService", CallbackContract=typeof(mdw.client.ChatService.IChatServiceCallback))]
    public interface IChatService {
        
        [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://tempuri.org/IChatService/SendMessage")]
        void SendMessage(string playerName, string message);
        
        [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://tempuri.org/IChatService/SendMessage")]
        System.Threading.Tasks.Task SendMessageAsync(string playerName, string message);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IChatService/Subscribe", ReplyAction="http://tempuri.org/IChatService/SubscribeResponse")]
        bool Subscribe(string playerName);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IChatService/Subscribe", ReplyAction="http://tempuri.org/IChatService/SubscribeResponse")]
        System.Threading.Tasks.Task<bool> SubscribeAsync(string playerName);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IChatService/Unsubscribe", ReplyAction="http://tempuri.org/IChatService/UnsubscribeResponse")]
        bool Unsubscribe();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IChatService/Unsubscribe", ReplyAction="http://tempuri.org/IChatService/UnsubscribeResponse")]
        System.Threading.Tasks.Task<bool> UnsubscribeAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IChatService/GetOnlinePlayers", ReplyAction="http://tempuri.org/IChatService/GetOnlinePlayersResponse")]
        string[] GetOnlinePlayers();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IChatService/GetOnlinePlayers", ReplyAction="http://tempuri.org/IChatService/GetOnlinePlayersResponse")]
        System.Threading.Tasks.Task<string[]> GetOnlinePlayersAsync();
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IChatServiceCallback {
        
        [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://tempuri.org/IChatService/onMessageSend")]
        void onMessageSend(System.DateTime dateTime, string playerName, string message);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IChatServiceChannel : mdw.client.ChatService.IChatService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class ChatServiceClient : System.ServiceModel.DuplexClientBase<mdw.client.ChatService.IChatService>, mdw.client.ChatService.IChatService {
        
        public ChatServiceClient(System.ServiceModel.InstanceContext callbackInstance) : 
                base(callbackInstance) {
        }
        
        public ChatServiceClient(System.ServiceModel.InstanceContext callbackInstance, string endpointConfigurationName) : 
                base(callbackInstance, endpointConfigurationName) {
        }
        
        public ChatServiceClient(System.ServiceModel.InstanceContext callbackInstance, string endpointConfigurationName, string remoteAddress) : 
                base(callbackInstance, endpointConfigurationName, remoteAddress) {
        }
        
        public ChatServiceClient(System.ServiceModel.InstanceContext callbackInstance, string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(callbackInstance, endpointConfigurationName, remoteAddress) {
        }
        
        public ChatServiceClient(System.ServiceModel.InstanceContext callbackInstance, System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(callbackInstance, binding, remoteAddress) {
        }
        
        public void SendMessage(string playerName, string message) {
            base.Channel.SendMessage(playerName, message);
        }
        
        public System.Threading.Tasks.Task SendMessageAsync(string playerName, string message) {
            return base.Channel.SendMessageAsync(playerName, message);
        }
        
        public bool Subscribe(string playerName) {
            return base.Channel.Subscribe(playerName);
        }
        
        public System.Threading.Tasks.Task<bool> SubscribeAsync(string playerName) {
            return base.Channel.SubscribeAsync(playerName);
        }
        
        public bool Unsubscribe() {
            return base.Channel.Unsubscribe();
        }
        
        public System.Threading.Tasks.Task<bool> UnsubscribeAsync() {
            return base.Channel.UnsubscribeAsync();
        }
        
        public string[] GetOnlinePlayers() {
            return base.Channel.GetOnlinePlayers();
        }
        
        public System.Threading.Tasks.Task<string[]> GetOnlinePlayersAsync() {
            return base.Channel.GetOnlinePlayersAsync();
        }
    }
}
