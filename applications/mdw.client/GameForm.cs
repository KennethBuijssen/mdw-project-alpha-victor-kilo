﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mdw.client
{
    public partial class GameForm : Form
    {
        private GameService.GameServiceClient gameServiceClient;
        private GameService.Game game;
        private GameService.SessionToken sessionToken;

        private ChatControl chatControl = new ChatControl();

        public GameForm(PortalService.GameRoom gameRoom, AuthenticationService.SessionToken sessionToken, bool creating = false)
        {
            InitializeComponent();

            chatControl.Top = this.Height - chatControl.Height - 50;
            this.Controls.Add(chatControl);

            gameServiceClient = new GameService.GameServiceClient();

            //convert between different object types (due to a bug with multiple WCF services)
            GameService.GameRoom gameServiceGameRoom = new GameService.GameRoom()
                                                        {
                                                            Addres          = gameRoom.Addres,
                                                            GameRoomInfo    = new GameService.GameRoomInfo()
                                                                                {
                                                                                    Name            = gameRoom.GameRoomInfo.Name,
                                                                                    CurrentPlayers  = gameRoom.GameRoomInfo.CurrentPlayers,
                                                                                    MaxPlayers      = gameRoom.GameRoomInfo.MaxPlayers,
                                                                                    Observable      = gameRoom.GameRoomInfo.Observable,
                                                                                    ExtensionData   = gameRoom.GameRoomInfo.ExtensionData
                                                                                },
                                                            ExtensionData   = gameRoom.ExtensionData
                                                        };
            this.sessionToken = new GameService.SessionToken() 
                                    {
                                        Email = sessionToken.Email, 
                                        Token = sessionToken.Token,
                                        ExtensionData = sessionToken.ExtensionData
                                    };

            //setup the connection with the gameservice
            if (creating)
            {
                game = gameServiceClient.createGame(gameServiceGameRoom, this.sessionToken);
            }
            else
            {
                game = gameServiceClient.joinGame(gameServiceGameRoom, this.sessionToken);
            }
        }

        private void btnPlay_Click(object sender, EventArgs e)
        {
            if (gameServiceClient.playGame(game, this.sessionToken))
            {
                MessageBox.Show("Round won");
            }
            else
            {
                MessageBox.Show("Round lost");
            }
        }

        private void btnLeave_Click(object sender, EventArgs e)
        {
            gameServiceClient.leaveGame(game, this.sessionToken);
            MessageBox.Show("Left game");
            this.Close();
        }
    }
}
