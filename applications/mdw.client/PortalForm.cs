﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ServiceModel;

namespace mdw.client
{
    public partial class PortalForm : Form, PortalService.IPortalServiceCallback
    {
        private AuthenticationControl authenticationControl;
        private ChatControl chatControl;
        private GameForm gameForm;

        public PortalForm()
        {
            InitializeComponent();
            portalClient = new PortalService.PortalServiceClient(new InstanceContext(this));

            dgvGameRooms.DataSource = gameRoomInfos;

            authenticationControl = new AuthenticationControl();
            this.Controls.Add(authenticationControl);

            chatControl = new ChatControl();
            chatControl.Top = this.Height - chatControl.Height - 50;
            this.Controls.Add(chatControl);
        }

        private PortalService.PortalServiceClient portalClient;
        private List<PortalService.GameRoomInfo> gameRoomInfos = new List<PortalService.GameRoomInfo>();

        public void gameRoomAdded(PortalService.GameRoomInfo gameRoomInfo)
        {
            gameRoomInfos.Add(gameRoomInfo);
            dgvGameRooms.DataSource = gameRoomInfos;
        }

        public void gameRoomRemoved(PortalService.GameRoomInfo gameRoomInfo)
        {
            gameRoomInfos.Remove(gameRoomInfo);
            dgvGameRooms.DataSource = gameRoomInfos;
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            gameRoomInfos = portalClient.GetAllGameRoomInformation().ToList<PortalService.GameRoomInfo>();
            dgvGameRooms.DataSource = gameRoomInfos;
        }

        private void btnJoin_Click(object sender, EventArgs e)
        {
            if (authenticationControl.sessionToken == null)
            {
                return;
            }

            PortalService.GameRoomInfo gameRoomInfo = (PortalService.GameRoomInfo)dgvGameRooms.CurrentRow.DataBoundItem;

            PortalService.GameRoom gameRoom = portalClient.JoinGameRoom(gameRoomInfo);

            gameForm = new GameForm(gameRoom, authenticationControl.sessionToken, false);
            gameForm.Show();
        }

        private void btnCreateGameRoom_Click(object sender, EventArgs e)
        {
            if (authenticationControl.sessionToken == null)
            {
                return;
            }

            PortalService.GameRoomInfo gameRoomInfo = new PortalService.GameRoomInfo() { Name = "Join Me for an legendary match!", MaxPlayers = 4, Observable = false };

            PortalService.GameRoom gameRoom = portalClient.CreateGameRoom(gameRoomInfo);

            gameForm = new GameForm(gameRoom, authenticationControl.sessionToken, true);
            gameForm.Show();
        }

        private void dgvGameRooms_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
