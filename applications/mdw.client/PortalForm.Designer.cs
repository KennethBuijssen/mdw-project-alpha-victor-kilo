﻿namespace mdw.client
{
    partial class PortalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCreateGameRoom = new System.Windows.Forms.Button();
            this.btnJoin = new System.Windows.Forms.Button();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.dgvGameRooms = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGameRooms)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCreateGameRoom
            // 
            this.btnCreateGameRoom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCreateGameRoom.Location = new System.Drawing.Point(1131, 371);
            this.btnCreateGameRoom.Name = "btnCreateGameRoom";
            this.btnCreateGameRoom.Size = new System.Drawing.Size(122, 23);
            this.btnCreateGameRoom.TabIndex = 7;
            this.btnCreateGameRoom.Text = "Create Game Room";
            this.btnCreateGameRoom.UseVisualStyleBackColor = true;
            this.btnCreateGameRoom.Click += new System.EventHandler(this.btnCreateGameRoom_Click);
            // 
            // btnJoin
            // 
            this.btnJoin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnJoin.Location = new System.Drawing.Point(1131, 342);
            this.btnJoin.Name = "btnJoin";
            this.btnJoin.Size = new System.Drawing.Size(122, 23);
            this.btnJoin.TabIndex = 6;
            this.btnJoin.Text = "Join";
            this.btnJoin.UseVisualStyleBackColor = true;
            this.btnJoin.Click += new System.EventHandler(this.btnJoin_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRefresh.Location = new System.Drawing.Point(1131, 313);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(122, 23);
            this.btnRefresh.TabIndex = 5;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // dgvGameRooms
            // 
            this.dgvGameRooms.AllowUserToAddRows = false;
            this.dgvGameRooms.AllowUserToDeleteRows = false;
            this.dgvGameRooms.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvGameRooms.Location = new System.Drawing.Point(645, 310);
            this.dgvGameRooms.Name = "dgvGameRooms";
            this.dgvGameRooms.ReadOnly = true;
            this.dgvGameRooms.Size = new System.Drawing.Size(480, 360);
            this.dgvGameRooms.TabIndex = 4;
            this.dgvGameRooms.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvGameRooms_CellContentClick);
            // 
            // PortalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 682);
            this.Controls.Add(this.btnCreateGameRoom);
            this.Controls.Add(this.btnJoin);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.dgvGameRooms);
            this.Name = "PortalForm";
            this.Text = "ApplicationForm";
            ((System.ComponentModel.ISupportInitialize)(this.dgvGameRooms)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCreateGameRoom;
        private System.Windows.Forms.Button btnJoin;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.DataGridView dgvGameRooms;


    }
}

