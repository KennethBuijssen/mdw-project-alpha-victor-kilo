﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mdw.client
{
    public partial class AuthenticationControl : UserControl
    {
        private AuthenticationService.AuthenticationServiceClient authenticationServiceClient;
        public AuthenticationService.SessionToken sessionToken = null;

        public AuthenticationControl()
        {
            InitializeComponent();
            authenticationServiceClient = new AuthenticationService.AuthenticationServiceClient();

            hideAuthenticationPanel();
            btnAuthentication.Text = "login";
        }

        private void hideAuthenticationPanel()
        {
            btnAuthentication.Text = "logout";
            pnlLoginLogout.Hide();
            btnAuthentication.Show();
        }

        private void showAuthenticationPanel()
        {
            pnlLoginLogout.Show();
            btnAuthentication.Hide();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (authenticationServiceClient == null)
            {
                return;
            }

            sessionToken = authenticationServiceClient.login(tbLoginEmail.Text, tbLoginPassword.Text);

            if (sessionToken != null)
            {
                MessageBox.Show("Login successfull");
                hideAuthenticationPanel();
            }
            else
            {
                MessageBox.Show("Login failed");
            }
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            if (sessionToken != null)
            {
                return;
            }

            sessionToken = authenticationServiceClient.registerAccount(tbRegisterUsername.Text, tbRegisterEmail.Text, tbRegisterPassword.Text);

            if (sessionToken != null)
            {
                MessageBox.Show("Register succesfull");
                hideAuthenticationPanel();
            }
            else
            {
                MessageBox.Show("Register failed");
            }
        }

        private void btnAuthentication_Click(object sender, EventArgs e)
        {
            if (sessionToken == null)
            {
                showAuthenticationPanel();
            }
            else
            {
                sessionToken = null;
                btnAuthentication.Text = "login";
            }
        }
    }
}
