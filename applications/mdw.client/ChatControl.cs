﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ServiceModel;

namespace mdw.client
{
    public partial class ChatControl : UserControl, ChatService.IChatServiceCallback
    {
        ChatService.ChatServiceClient proxy;
        InstanceContext context;

        public ChatControl()
        {
            InitializeComponent();

            context = new InstanceContext(this);
            proxy = new ChatService.ChatServiceClient(context);
            proxy.Subscribe("Veselin");


            foreach (var item in proxy.GetOnlinePlayers())
            {
                listBox1.Items.Add(item);
            }
        }

        void ChatService.IChatServiceCallback.onMessageSend(DateTime dateTime, string playerName, string message)
        {
            richTextBox2.AppendText(playerName + message + "\n");
        }

        public bool Subscribe()
        {
            return true;
        }

        public bool Unsubscribe()
        {
            return false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            proxy.SendMessage(" Dingo :  ", richTextBox1.Text);
        }
    }
}
