﻿namespace mdw.client
{
    partial class AuthenticationControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlLogin = new System.Windows.Forms.Panel();
            this.lblLoginPassword = new System.Windows.Forms.Label();
            this.lblLoginEmail = new System.Windows.Forms.Label();
            this.tbLoginPassword = new System.Windows.Forms.TextBox();
            this.tbLoginEmail = new System.Windows.Forms.TextBox();
            this.btnLogin = new System.Windows.Forms.Button();
            this.lblLogin = new System.Windows.Forms.Label();
            this.pnlRegister = new System.Windows.Forms.Panel();
            this.lblRegisterPassword = new System.Windows.Forms.Label();
            this.lblRegisterUsername = new System.Windows.Forms.Label();
            this.lblRegisterEmail = new System.Windows.Forms.Label();
            this.tbRegisterUsername = new System.Windows.Forms.TextBox();
            this.tbRegisterPassword = new System.Windows.Forms.TextBox();
            this.tbRegisterEmail = new System.Windows.Forms.TextBox();
            this.btnRegister = new System.Windows.Forms.Button();
            this.lblRegister = new System.Windows.Forms.Label();
            this.btnAuthentication = new System.Windows.Forms.Button();
            this.pnlLoginLogout = new System.Windows.Forms.Panel();
            this.pnlLogin.SuspendLayout();
            this.pnlRegister.SuspendLayout();
            this.pnlLoginLogout.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlLogin
            // 
            this.pnlLogin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlLogin.Controls.Add(this.lblLoginPassword);
            this.pnlLogin.Controls.Add(this.lblLoginEmail);
            this.pnlLogin.Controls.Add(this.tbLoginPassword);
            this.pnlLogin.Controls.Add(this.tbLoginEmail);
            this.pnlLogin.Controls.Add(this.btnLogin);
            this.pnlLogin.Controls.Add(this.lblLogin);
            this.pnlLogin.Location = new System.Drawing.Point(0, 0);
            this.pnlLogin.Name = "pnlLogin";
            this.pnlLogin.Size = new System.Drawing.Size(240, 360);
            this.pnlLogin.TabIndex = 0;
            // 
            // lblLoginPassword
            // 
            this.lblLoginPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoginPassword.Location = new System.Drawing.Point(40, 160);
            this.lblLoginPassword.Name = "lblLoginPassword";
            this.lblLoginPassword.Size = new System.Drawing.Size(160, 23);
            this.lblLoginPassword.TabIndex = 4;
            this.lblLoginPassword.Text = "Password:";
            this.lblLoginPassword.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // lblLoginEmail
            // 
            this.lblLoginEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoginEmail.Location = new System.Drawing.Point(40, 100);
            this.lblLoginEmail.Name = "lblLoginEmail";
            this.lblLoginEmail.Size = new System.Drawing.Size(160, 23);
            this.lblLoginEmail.TabIndex = 1;
            this.lblLoginEmail.Text = "Email:";
            this.lblLoginEmail.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // tbLoginPassword
            // 
            this.tbLoginPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbLoginPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbLoginPassword.Location = new System.Drawing.Point(40, 185);
            this.tbLoginPassword.Name = "tbLoginPassword";
            this.tbLoginPassword.PasswordChar = '*';
            this.tbLoginPassword.Size = new System.Drawing.Size(160, 29);
            this.tbLoginPassword.TabIndex = 3;
            // 
            // tbLoginEmail
            // 
            this.tbLoginEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbLoginEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbLoginEmail.Location = new System.Drawing.Point(40, 125);
            this.tbLoginEmail.Name = "tbLoginEmail";
            this.tbLoginEmail.Size = new System.Drawing.Size(160, 29);
            this.tbLoginEmail.TabIndex = 2;
            // 
            // btnLogin
            // 
            this.btnLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogin.Location = new System.Drawing.Point(20, 300);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(200, 40);
            this.btnLogin.TabIndex = 2;
            this.btnLogin.Text = "Login";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // lblLogin
            // 
            this.lblLogin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLogin.Location = new System.Drawing.Point(0, 0);
            this.lblLogin.Name = "lblLogin";
            this.lblLogin.Size = new System.Drawing.Size(240, 80);
            this.lblLogin.TabIndex = 1;
            this.lblLogin.Text = "Login";
            this.lblLogin.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // pnlRegister
            // 
            this.pnlRegister.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlRegister.Controls.Add(this.lblRegisterPassword);
            this.pnlRegister.Controls.Add(this.lblRegisterUsername);
            this.pnlRegister.Controls.Add(this.lblRegisterEmail);
            this.pnlRegister.Controls.Add(this.tbRegisterUsername);
            this.pnlRegister.Controls.Add(this.tbRegisterPassword);
            this.pnlRegister.Controls.Add(this.tbRegisterEmail);
            this.pnlRegister.Controls.Add(this.btnRegister);
            this.pnlRegister.Controls.Add(this.lblRegister);
            this.pnlRegister.Location = new System.Drawing.Point(240, 0);
            this.pnlRegister.Name = "pnlRegister";
            this.pnlRegister.Size = new System.Drawing.Size(240, 360);
            this.pnlRegister.TabIndex = 1;
            // 
            // lblRegisterPassword
            // 
            this.lblRegisterPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegisterPassword.Location = new System.Drawing.Point(40, 220);
            this.lblRegisterPassword.Name = "lblRegisterPassword";
            this.lblRegisterPassword.Size = new System.Drawing.Size(160, 23);
            this.lblRegisterPassword.TabIndex = 8;
            this.lblRegisterPassword.Text = "Password:";
            this.lblRegisterPassword.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // lblRegisterUsername
            // 
            this.lblRegisterUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegisterUsername.Location = new System.Drawing.Point(40, 160);
            this.lblRegisterUsername.Name = "lblRegisterUsername";
            this.lblRegisterUsername.Size = new System.Drawing.Size(160, 23);
            this.lblRegisterUsername.TabIndex = 7;
            this.lblRegisterUsername.Text = "Username:";
            this.lblRegisterUsername.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // lblRegisterEmail
            // 
            this.lblRegisterEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegisterEmail.Location = new System.Drawing.Point(40, 100);
            this.lblRegisterEmail.Name = "lblRegisterEmail";
            this.lblRegisterEmail.Size = new System.Drawing.Size(160, 23);
            this.lblRegisterEmail.TabIndex = 5;
            this.lblRegisterEmail.Text = "Email:";
            this.lblRegisterEmail.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // tbRegisterUsername
            // 
            this.tbRegisterUsername.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbRegisterUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbRegisterUsername.Location = new System.Drawing.Point(40, 185);
            this.tbRegisterUsername.Name = "tbRegisterUsername";
            this.tbRegisterUsername.Size = new System.Drawing.Size(160, 26);
            this.tbRegisterUsername.TabIndex = 6;
            // 
            // tbRegisterPassword
            // 
            this.tbRegisterPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbRegisterPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbRegisterPassword.Location = new System.Drawing.Point(40, 245);
            this.tbRegisterPassword.Name = "tbRegisterPassword";
            this.tbRegisterPassword.PasswordChar = '*';
            this.tbRegisterPassword.Size = new System.Drawing.Size(160, 26);
            this.tbRegisterPassword.TabIndex = 5;
            // 
            // tbRegisterEmail
            // 
            this.tbRegisterEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbRegisterEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbRegisterEmail.Location = new System.Drawing.Point(40, 125);
            this.tbRegisterEmail.Name = "tbRegisterEmail";
            this.tbRegisterEmail.Size = new System.Drawing.Size(160, 26);
            this.tbRegisterEmail.TabIndex = 4;
            // 
            // btnRegister
            // 
            this.btnRegister.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRegister.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegister.Location = new System.Drawing.Point(20, 300);
            this.btnRegister.Name = "btnRegister";
            this.btnRegister.Size = new System.Drawing.Size(200, 40);
            this.btnRegister.TabIndex = 3;
            this.btnRegister.Text = "Register";
            this.btnRegister.UseVisualStyleBackColor = true;
            this.btnRegister.Click += new System.EventHandler(this.btnRegister_Click);
            // 
            // lblRegister
            // 
            this.lblRegister.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblRegister.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegister.Location = new System.Drawing.Point(0, 0);
            this.lblRegister.Name = "lblRegister";
            this.lblRegister.Size = new System.Drawing.Size(240, 80);
            this.lblRegister.TabIndex = 2;
            this.lblRegister.Text = "Register";
            this.lblRegister.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // btnAuthentication
            // 
            this.btnAuthentication.BackColor = System.Drawing.Color.White;
            this.btnAuthentication.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAuthentication.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAuthentication.Location = new System.Drawing.Point(637, 3);
            this.btnAuthentication.Name = "btnAuthentication";
            this.btnAuthentication.Size = new System.Drawing.Size(80, 40);
            this.btnAuthentication.TabIndex = 2;
            this.btnAuthentication.Text = "Login";
            this.btnAuthentication.UseVisualStyleBackColor = false;
            this.btnAuthentication.Click += new System.EventHandler(this.btnAuthentication_Click);
            // 
            // pnlLoginLogout
            // 
            this.pnlLoginLogout.BackColor = System.Drawing.Color.White;
            this.pnlLoginLogout.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlLoginLogout.Controls.Add(this.pnlLogin);
            this.pnlLoginLogout.Controls.Add(this.pnlRegister);
            this.pnlLoginLogout.Location = new System.Drawing.Point(3, 3);
            this.pnlLoginLogout.Name = "pnlLoginLogout";
            this.pnlLoginLogout.Size = new System.Drawing.Size(480, 360);
            this.pnlLoginLogout.TabIndex = 9;
            // 
            // AuthenticationControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.pnlLoginLogout);
            this.Controls.Add(this.btnAuthentication);
            this.Name = "AuthenticationControl";
            this.Size = new System.Drawing.Size(720, 366);
            this.pnlLogin.ResumeLayout(false);
            this.pnlLogin.PerformLayout();
            this.pnlRegister.ResumeLayout(false);
            this.pnlRegister.PerformLayout();
            this.pnlLoginLogout.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlRegister;
        private System.Windows.Forms.Label lblRegister;
        private System.Windows.Forms.Panel pnlLogin;
        private System.Windows.Forms.Label lblLogin;
        private System.Windows.Forms.Label lblLoginPassword;
        private System.Windows.Forms.Label lblLoginEmail;
        private System.Windows.Forms.TextBox tbLoginPassword;
        private System.Windows.Forms.TextBox tbLoginEmail;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.Label lblRegisterPassword;
        private System.Windows.Forms.Label lblRegisterUsername;
        private System.Windows.Forms.Label lblRegisterEmail;
        private System.Windows.Forms.TextBox tbRegisterUsername;
        private System.Windows.Forms.TextBox tbRegisterPassword;
        private System.Windows.Forms.TextBox tbRegisterEmail;
        private System.Windows.Forms.Button btnRegister;
        private System.Windows.Forms.Button btnAuthentication;
        private System.Windows.Forms.Panel pnlLoginLogout;
    }
}
