﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace mdw.service.chat
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IChatService" in both code and config file together.
    [ServiceContract(CallbackContract = typeof(IChatCallback))]
    public interface IChatService
    {
        [OperationContract(IsOneWay = true)]
        void SendMessage(string playerName, string message);

        [OperationContract]
        bool Subscribe(string playerName);

        [OperationContract]
        bool Unsubscribe();

        [OperationContract]
        List<String> GetOnlinePlayers();
    }

    public interface IChatCallback
    {
        [OperationContract(IsOneWay = true)]
        void onMessageSend(DateTime dateTime, string playerName, string message);
    }
}
