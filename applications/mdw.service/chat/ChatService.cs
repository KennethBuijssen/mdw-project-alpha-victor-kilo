﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace mdw.service.chat
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ChatService" in both code and config file together.
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Reentrant)]
    public class ChatService : IChatService
    {


        //private List<Player> playerList;
        private static List<IChatCallback> subscribers = new List<IChatCallback>();
        private List<String> onlinePlayers = new List<String>();

        public ChatService()
        {
            // playerList = new List<Player>();
        }

        public bool Subscribe(string playerName)
        {
            try
            {
                IChatCallback chatCallBack = OperationContext.Current.GetCallbackChannel<IChatCallback>();
                if (!subscribers.Contains(chatCallBack))

                    subscribers.Add(chatCallBack);
                onlinePlayers.Add(playerName);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Unsubscribe()
        {
            try
            {
                IChatCallback callback = OperationContext.Current.GetCallbackChannel<IChatCallback>();
                if (subscribers.Contains(callback))
                    subscribers.Remove(callback);
                return true;
            }
            catch
            {
                return false;
            }

        }

        public void SendMessage(string playerName, string message)
        {
            subscribers.ForEach(delegate(IChatCallback callback)
            {
                if (((ICommunicationObject)callback).State == CommunicationState.Opened)
                {
                    callback.onMessageSend(DateTime.Now, playerName, message);
                }
                else
                {
                    subscribers.Remove(callback);
                }
            });
        }

        public List<String> GetOnlinePlayers()
        {
            return onlinePlayers;
        }

    }
}
