﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace mdw.service.portal
{
    [DataContract]
    public class GameRoomInfo
    {
        [DataMember]
        static int nextFreeID = 0;

        [DataMember]
        public int ID { get; private set; }

        [DataMember]
        public string Name;

        [DataMember]
        public int CurrentPlayers;

        [DataMember]
        public int MaxPlayers;

        [DataMember]
        public bool Observable;

        public GameRoomInfo(string name, int maxPlayers, bool observable)
        {
            ID = nextFreeID++;
            Name = name;
            CurrentPlayers = 0;
            MaxPlayers = maxPlayers;
            Observable = observable;
        }
    }
}
