﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace mdw.service.portal
{
    [DataContract]
    public class GameRoom
    {
        [DataMember]
        public string Addres { get; private set; }

        [DataMember]
        public GameRoomInfo GameRoomInfo { get; private set; }

        public GameRoom(string address, GameRoomInfo gameRoomInfo)
        {
            Addres = address;
            GameRoomInfo = gameRoomInfo;
        }
    }
}
