﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace mdw.service.portal
{
    [ServiceContract(CallbackContract = typeof(IPortalCallback))]
    public interface IPortalService
    {
        [OperationContract]
        List<GameRoomInfo> GetAllGameRoomInformation();

        [OperationContract]
        GameRoom CreateGameRoom(GameRoomInfo gameRoomInfo);

        [OperationContract]
        GameRoom JoinGameRoom(GameRoomInfo gameRoomInfo);

        [OperationContract]
        void SubscribeGameRoomUpdates();

        [OperationContract]
        void UnsubscribeGameRoomUpdates();
    }

    public interface IPortalCallback
    {
        [OperationContract]
        void gameRoomAdded(GameRoomInfo gameRoomInfo);

        [OperationContract]
        void gameRoomRemoved(GameRoomInfo gameRoomInfo);
    }
}
