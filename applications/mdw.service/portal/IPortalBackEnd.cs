﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace mdw.service.portal
{
    [ServiceContract]
    public interface IPortalBackEnd
    {
        [OperationContract]
        void RegisterGameRoom(GameRoom gameRoom);

        [OperationContract]
        void DeregisterGameRoom(GameRoom gameRoom);

        [OperationContract]
        void UpdateGameRoom(GameRoom gameRoom);
    }
}
