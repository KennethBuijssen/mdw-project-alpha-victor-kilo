﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace mdw.service.portal
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class PortalService : IPortalService, IPortalBackEnd
    {
        private Action<GameRoomInfo> gameRoomAddedEvent = delegate { };
        private Action<GameRoomInfo> gameRoomRemovedEvent = delegate { };
        private List<GameRoom> gameRooms = new List<GameRoom>();

        public List<GameRoomInfo> GetAllGameRoomInformation()
        {
            List<GameRoomInfo> gameRoomInfos = new List<GameRoomInfo>();

            foreach (GameRoom gameRoom in gameRooms)
            {
                gameRoomInfos.Add(gameRoom.GameRoomInfo);
            }

            return gameRoomInfos;
        }

        public GameRoom CreateGameRoom(GameRoomInfo gameRoomInfo)
        {
            return new GameRoom(null, gameRoomInfo);
        }

        public GameRoom JoinGameRoom(GameRoomInfo gameRoomInfo)
        {
            foreach (GameRoom gameRoom in gameRooms)
            {
                if (gameRoom.GameRoomInfo.ID == gameRoomInfo.ID)
                {
                    return gameRoom;
                }
            }

            return null;
        }

        public void SubscribeGameRoomUpdates()
        {
            IPortalCallback subscriber = OperationContext.Current.GetCallbackChannel<IPortalCallback>();

            gameRoomAddedEvent += subscriber.gameRoomAdded;
            gameRoomAddedEvent += subscriber.gameRoomRemoved;
        }

        public void UnsubscribeGameRoomUpdates()
        {
            IPortalCallback subscriber = OperationContext.Current.GetCallbackChannel<IPortalCallback>();

            gameRoomAddedEvent -= subscriber.gameRoomAdded;
            gameRoomAddedEvent -= subscriber.gameRoomRemoved;
        }

        public void RegisterGameRoom(GameRoom gameRoom)
        {
            if (!gameRooms.Contains(gameRoom))
            {
                gameRooms.Add(gameRoom);
                gameRoomAddedEvent(gameRoom.GameRoomInfo);
            }
        }

        public void DeregisterGameRoom(GameRoom gameRoom)
        {
            if (gameRooms.Remove(gameRoom))
            {
                gameRoomRemovedEvent(gameRoom.GameRoomInfo);
            }
        }

        public void UpdateGameRoom(GameRoom gameRoom)
        {
            int index = gameRooms.IndexOf(gameRoom);

            gameRoomRemovedEvent(gameRooms[index].GameRoomInfo);
            gameRooms[index] = gameRoom;
            gameRoomAddedEvent(gameRoom.GameRoomInfo);
        }
    }
}
