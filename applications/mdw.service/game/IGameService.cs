﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace mdw.service.game
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IGameService" in both code and config file together.
    [ServiceContract]
    public interface IGameService
    {
        [OperationContract]
        Game createGame(PortalService.GameRoom gameRoom, AuthenticationService.SessionToken sessionToken);

        [OperationContract]
        Game joinGame(PortalService.GameRoom gameRoom, AuthenticationService.SessionToken sessionToken);

        [OperationContract]
        void leaveGame(Game game, AuthenticationService.SessionToken sessionToken);

        [OperationContract]
        bool playGame(Game game, AuthenticationService.SessionToken sessionToken);
    }
}
