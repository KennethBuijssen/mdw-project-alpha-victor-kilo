﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace mdw.service.game
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class GameService : IGameService
    {
        private List<Game> games = new List<Game>();

        private AuthenticationService.AuthenticationBackendClient authenticationBackendClient = new AuthenticationService.AuthenticationBackendClient();
        private PortalService.PortalBackEndClient portalBackendClient = new PortalService.PortalBackEndClient();

        public Game createGame(PortalService.GameRoom gameRoom, AuthenticationService.SessionToken sessionToken)
        {
            //force the client to have a valid sessionToken provided
            forceVallidSessionToken(sessionToken);

            //create a new game
            Game game = new Game(portalBackendClient, gameRoom);
            games.Add(game);
            
            //join the newly created game
            return joinGame(gameRoom, sessionToken);
        }

        public Game joinGame(PortalService.GameRoom gameRoom, AuthenticationService.SessionToken sessionToken)
        {
            //force the client to have a valid sessionToken provided
            forceVallidSessionToken(sessionToken);

            //find the right gameroom
            foreach (Game game in games)
            {
                if (game.gameRoom == gameRoom)
                {
                    if (game.Join(sessionToken))
                    {
                        return game;
                    }
                }
            }

            //return null if gameroom could not be found
            return null;
        }

        public void leaveGame(Game game, AuthenticationService.SessionToken sessionToken)
        {
            //force the client to have a valid sessionToken provided
            forceVallidSessionToken(sessionToken);

            game.Leave(sessionToken);

            //remove it from the list if no players are left
            if (game.gameRoom.GameRoomInfo.CurrentPlayers == 0)
            {
                games.Remove(game);
            }
        }

        public bool playGame(Game game, AuthenticationService.SessionToken sessionToken)
        {
            //force the client to have a valid sessionToken provided
            forceVallidSessionToken(sessionToken);

            return game.Play(sessionToken);
        }

        private void forceVallidSessionToken(AuthenticationService.SessionToken sessionToken)
        {
            if (!authenticationBackendClient.isVallidSessionToken(sessionToken))
            {
                throw new FaultException();
            }
        }
    }
}
