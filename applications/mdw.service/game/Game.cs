﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace mdw.service.game
{
    [DataContract]
    public class Game
    {
        [DataMember]
        static private int nextFreeID = 0;

        [DataMember]
        private PortalService.PortalBackEndClient portalBackend;

        [DataMember]
        private List<AuthenticationService.SessionToken> sessionTokens;

        [DataMember]
        public int ID { get; private set; }
        
        [DataMember]
        public PortalService.GameRoom gameRoom { get; private set; }

        public Game(PortalService.PortalBackEndClient portalBackend, PortalService.GameRoom gameRoom)
        {
            //assign a unique ID to each game
            ID = nextFreeID++;

            this.portalBackend = portalBackend;
            this.gameRoom = gameRoom;

            this.portalBackend.RegisterGameRoom(this.gameRoom);
            this.sessionTokens = new List<AuthenticationService.SessionToken>();
        }

        public bool Join(AuthenticationService.SessionToken sessionToken)
        {
            if (gameRoom.GameRoomInfo.CurrentPlayers < gameRoom.GameRoomInfo.MaxPlayers)
            {
                gameRoom.GameRoomInfo.CurrentPlayers++;
                sessionTokens.Add(sessionToken);
                return true;
            }

            return false;
        }

        public void Leave(AuthenticationService.SessionToken sessionToken)
        {
            bool playerIsVallid = false;
            foreach (AuthenticationService.SessionToken token in sessionTokens)
            {
                if (sessionToken.Email == token.Email && sessionToken.Token == token.Token)
                {
                    playerIsVallid = true;
                }
            }

            //make sure the player is allowed to play in this game
            if (!playerIsVallid)
            {
                throw new System.ServiceModel.FaultException();
            }

            gameRoom.GameRoomInfo.CurrentPlayers--;
        }

        public bool Play(AuthenticationService.SessionToken sessionToken)
        {
            bool playerIsVallid = false;
            foreach (AuthenticationService.SessionToken token in sessionTokens)
            {
                if (sessionToken.Email == token.Email && sessionToken.Token == token.Token)
                {
                    playerIsVallid = true;
                }
            }
            
            //make sure the player is allowed to play in this game
            if (!playerIsVallid)
            {
                throw new System.ServiceModel.FaultException();
            }

            //return if the result of the turn
            if (System.DateTime.Now.Ticks % 2 == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        ~Game()
        {
            this.portalBackend.DeregisterGameRoom(this.gameRoom);
        }
    }
}
