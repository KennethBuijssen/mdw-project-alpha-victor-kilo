﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace mdw.service.authentication
{
    [ServiceContract]
    public interface IAuthenticationService
    {
        [OperationContract]
        SessionToken registerAccount(string userName, string email, string password);

        [OperationContract]
        SessionToken login(string email, string password);

        [OperationContract]
        void logout(SessionToken sessionToken);

        [OperationContract]
        bool changePassword(string email, string Password, string NewPassword);
    }
}
