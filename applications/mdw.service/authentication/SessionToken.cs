﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace mdw.service.authentication
{
    [DataContract]
    public class SessionToken
    {
        /// <summary>
        /// The email adress for the account matching the session
        /// </summary>
        [DataMember]
        public string Email { get; private set; }

        /// <summary>
        /// The token identifing the session
        /// </summary>
        [DataMember]
        public string Token { get; private set; }


        /// <summary>
        /// Created a new session token
        /// </summary>
        /// <param name="email">The email for the account matching the session token</param>
        /// <param name="token">The token identifing the session</param>
        public SessionToken(string email, string token)
        {
            Token = token;
            Email = email;
        }
    }
}
