﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace mdw.service.authentication
{
    [ServiceContract]
    public interface IAuthenticationBackend
    {
        [OperationContract]
        bool isVallidSessionToken(SessionToken sessionToken);
    }
}
