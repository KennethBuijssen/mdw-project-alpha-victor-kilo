﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mdw.service.authentication
{
    class Account
    {
        public string UserName { get; private set; }
        public string Email { get; private set; }
        private string Password { get; set; }

        /// <summary>
        /// A list of vallid sessionTokens for the account
        /// </summary>
        private List<SessionToken> sessionTokens = new List<SessionToken>();


        /// <summary>
        /// Create a new account
        /// </summary>
        /// <param name="email">The email address for the account</param>
        /// <param name="userName">The accounts username</param>
        /// <param name="password">The accounts password</param>
        public Account(string email, string userName, string password)
        {
            Email = email;
            UserName = userName;
            Password = password;
        }

        /// <summary>
        /// Check if the specified password is vallid for the account
        /// </summary>
        /// <param name="password">The password to check</param>
        /// <returns>Returns true if the password was valid, otherwise false</returns>
        public bool checkPassword(string password)
        {
            return Password == password;
        }

        /// <summary>
        /// Change the accounts password
        /// </summary>
        /// <param name="password">The accounts current password</param>
        /// <param name="newPassword">The accounts new password</param>
        /// <returns>True if the password was changed, otherwise false</returns>
        public bool changePassword(string password, string newPassword)
        {
            if (Password != password)
            {
                return false;
            }

            Password = newPassword;
            return true;
        }

        /// <summary>
        /// Adds a new sessionToken to the account
        /// </summary>
        /// <param name="sessionToken">The session token to add</param>
        /// <returns>Returns true if the sessionToken was added, otherwise false</returns>
        public bool addSessionToken(SessionToken sessionToken)
        {
            if (sessionToken.Email != Email)
            {
                return false;
            }

            sessionTokens.Add(sessionToken);
            return true;
        }

        /// <summary>
        /// Remove the given sessionToken from the account
        /// </summary>
        /// <param name="sessionToken">The sessionToken to remove</param>
        public void removeSessionToken(SessionToken sessionToken)
        {
            sessionTokens.Remove(sessionToken);
        }

        /// <summary>
        /// Check if the given sessionToken is vallid for the account
        /// </summary>
        /// <param name="sessionToken">The sessionToken to check</param>
        /// <returns>True if the sessionToken is vallid, otherwise false</returns>
        public bool isVallidSessionToken(SessionToken sessionToken)
        {
            //check email
            if (sessionToken.Email != Email)
            {
                return false;
            }

            //check if the session token matches
            foreach (SessionToken item in sessionTokens)
            {
                if (item.Token == sessionToken.Token)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
