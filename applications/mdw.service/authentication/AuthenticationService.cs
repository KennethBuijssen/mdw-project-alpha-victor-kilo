﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace mdw.service.authentication
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class AuthenticationService : IAuthenticationService, IAuthenticationBackend
    {
        /// <summary>
        /// A list containing all accounts registred to the authentication service
        /// </summary>
        private List<Account> accounts = new List<Account>();

        /// <summary>
        /// Registers a new account to the authentication service
        /// </summary>
        /// <param name="userName">The unique username address for the new account</param>
        /// <param name="email">The unique email address for the new account</param>
        /// <param name="password">The password for the new account</param>
        /// <returns>A session token for the newly created account or null if registration failed</returns>
        public SessionToken registerAccount(string userName, string email, string password)
        {
            //check for duplicate usernames and emails
            foreach (Account account in accounts)
            {
                if (account.UserName == userName || account.Email == email)
                {
                    return null;
                }
            }

            //add the new account to the system
            Account newAccount = new Account(email, userName, password);
            accounts.Add(newAccount);

            //automatically login the new account
            return login(email, password);
        }

        /// <summary>
        /// Login to an exisiting account
        /// </summary>
        /// <param name="email">The email for the account</param>
        /// <param name="password">Tje password for the account</param>
        /// <returns>A session token for the account, or null if login failed</returns>
        public SessionToken login(string email, string password)
        {
            Account account = findAccount(email);

            //make sure an account was found
            if (account == null)
            {
                return null;
            }
            else if (!account.checkPassword(password)) //check for invallid password
            {
                return null;
            }

            //create a sessionToken with a simple Guid
            SessionToken sessionToken = new SessionToken(email, Guid.NewGuid().ToString("n").Substring(0, 32));
            account.addSessionToken(sessionToken);

            return sessionToken;
        }

        /// <summary>
        /// Ends the session matching the given sessionToken
        /// </summary>
        /// <param name="sessionToken">The sessiontoken specifiying which session to logout from</param>
        public void logout(SessionToken sessionToken)
        {
            foreach (Account account in accounts)
            {
                account.removeSessionToken(sessionToken);
            }
        }

        /// <summary>
        /// Change the password of an account
        /// </summary>
        /// <param name="email">The email address for the account</param>
        /// <param name="Password">The accounts current password</param>
        /// <param name="NewPassword">The accounts new password</param>
        /// <returns>A boolean specifing of the password change was successfull</returns>
        public bool changePassword(string email, string Password, string NewPassword)
        {
            Account account = findAccount(email);

            //make sure an account was found
            if (account == null)
            {
                return false;
            }

            return account.changePassword(Password, NewPassword);
        }

        /// <summary>
        /// Checks if the specified sessionToken matches a valid session
        /// </summary>
        /// <param name="sessionToken">The session token to validate</param>
        /// <returns>True if the session was valid, otherwise false</returns>
        public bool isVallidSessionToken(SessionToken sessionToken)
        {
            if (sessionToken == null)
            {
                return false;
            }

            Account account = findAccount(sessionToken.Email);

            //check if the account exists
            if (account == null)
            {
                return false;
            }

            return account.isVallidSessionToken(sessionToken);
        }

        /// <summary>
        /// Given an email find an account in the account list
        /// </summary>
        /// <param name="email">The accounts email address</param>
        /// <returns>The account, if no account was found returns null</returns>
        private Account findAccount(string email)
        {
            foreach (Account account in accounts)
            {
                //check if the email matches the account
                if (account.Email == email)
                {
                    //return the account
                    return account;
                }
            }

            //null if no account was found
            return null;
        }
    }
}
