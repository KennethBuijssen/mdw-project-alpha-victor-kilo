[Player] ^ [Players]
[Player] - Store a game for playback
[Player] - Run a full player turn
[Spectator] - The user spectates a game
[Players] - Resume game
[Players] - Play a full game of snakes and ladders
Play a full game of snakes and ladders < Resume game
Play a full game of snakes and ladders < The user spectates a game
Play a full game of snakes and ladders > Run a full player turn
Store a game for playback